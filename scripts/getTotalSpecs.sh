#!/bin/bash
echo "Total available RAM:"
free -m | grep Mem | awk '{print $2}'
echo -e "\nTotal available CPU:"
lscpu | grep "^CPU(s):" | awk '{print $2}'