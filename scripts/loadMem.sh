#!/bin/bash

# Počítá podíl mezi aktuálním zatížením Memory a celkově dostupnou Memory.
# Výsledek je přepočítán na procenta a uložen do souboru,
# který byl předaný jako parametr $1
free | grep Mem | awk '{print $3/$2 * 100.0}' >> $1 

