#!/bin/bash

# Získá celkový počet dostupných CPU containeru (1024 = 1)
numberOfCpus=$(cat /sys/fs/cgroup/cpu/cpu.shares | awk '{print substr($0,1,1)}')

# Parsuje výstup z příkazu "ps -A -o pcpu" 
# Ukládá celkovou sumu do proměnné temp
temp=$(ps -A -o pcpu | tail -n+2 | paste -sd+ | bc) 

# Vypočítá průměrné vytížení CPU 
# Zapíná možnost desetinných míst v příkazu bc,
# který je potřeba pro výpočet průměrné hodnoty
temp=$(bc -l <<< "$temp"/$numberOfCpus)

# Výstup z příkazu "bc" je vypsaný s přesností na 2 desetinná místa 
# a uložen do souboru, který byl předaný jako parametr $1
echo $temp | awk '{printf "%.2f \n", $1}' >> $1



