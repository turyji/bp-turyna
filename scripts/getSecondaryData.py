import sys
import time
import subprocess
import os 

def getData(param):
    counter = 0
    dir = os.path.abspath("scripts")
    script = "loadCpu.sh"
    script2 = "loadMem.sh"
    script = os.path.join(dir, script)
    script2 = os.path.join(dir, script2)

    # sys.argv[1] = AWS/gitlab + ${CODEBUILD_BUILD_NUMBER} nebo $CI_PIPELINE_ID"
    file = "Gitlab-cpu-" + sys.argv[1] + ".csv" 
    file2 = "Gitlab-mem-" + sys.argv[1] + ".csv"

    with open(file, 'a+') as f, open(file2, 'a+') as f2: 
        while (counter <= int(param)):
        # Spouští bash script loadCpu.sh nebo loadMem.sh a předává parametr file
            subprocess.call(script + " " + file, shell=True) 
            subprocess.call(script2 + " " + file2, shell=True)
            time.sleep(1)
            counter += 1
    
if __name__ == "__main__":    
    # Spouští script po dobu 60s
    getData(60)



    
