import json
import sys
from datetime import datetime

def getPrimaryMetrics(jobId):
    with open('/home/jturyna/bp-turyna/data/primaryMetrics/' + jobId + '.json') as f:
        jobData = json.load(f)

    startTime = jobData['builds'][0]['startTime']
    endTime = jobData['builds'][0]['endTime']   
    for build in jobData['builds']:
        for phase in build['phases']:
            if phase['phaseType'] == 'QUEUED':
                queued_time = datetime.fromisoformat(phase['endTime']) 
                - datetime.fromisoformat(phase['startTime'])
                queued_time_str = str(queued_time.total_seconds())[:6]

    duration = datetime.fromisoformat(endTime) - datetime.fromisoformat(startTime)
    duration_str = str(duration.total_seconds())[:6]
    print("Duration: " + str(duration_str) + " secs")
    print("Queued time: " + str(queued_time_str) + " secs")

if __name__ == "__main__":
    getPrimaryMetrics(sys.argv[1])

