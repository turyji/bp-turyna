import requests
from datetime import datetime, timedelta

url = "https://gitlab.com/api/v4/projects/ZÁMĚRNĚ_VYMAZÁNNO/jobs?per_page=100"
headers = {"PRIVATE-TOKEN": "ZÁMĚRNĚ_VYMAZÁNO"}

params = {"per_page": 100}
response = requests.get(url, headers=headers, params=params)
response = response.json()

sortedJobs = sorted(response, key=lambda x: x.get("queued_duration", 0) 
                    if x.get("queued_duration") is not None else 0, reverse=True)

sevenDaysAgo = datetime.utcnow() - timedelta(days=7)

recentJobs = [job for job in sortedJobs 
              if datetime.fromisoformat(job["created_at"][:-1]) >= sevenDaysAgo]

while len(response) == 100:
    last_job_id = response[-1]["id"]
    params["page"] = params.get("page", 1) + 1
    response = requests.get(url, headers=headers, params=params)
    response = response.json()
    sortedJobs = sorted(response, key=lambda x: x.get("queued_duration", 0) 
                        if x.get("queued_duration") is not None else 0, reverse=True)
    recentJobs.extend([job for job in sortedJobs 
                       if datetime.fromisoformat(job["created_at"][:-1]) >= sevenDaysAgo])

totalJobs = len(recentJobs)
totalDuration = sum(job["duration"] or 0 for job in recentJobs)
totalQueuedDuration = sum(job["queued_duration"] or 0 for job in recentJobs)

oldestJob = min(recentJobs, key=lambda job: job["created_at"])
oldestJobDate = oldestJob["created_at"]
oldestJobId = oldestJob["id"]

print("Total number of jobs: " + str(totalJobs))
print("Total duration of jobs: " + str(round(totalDuration, 2)) + " seconds")
print("Total queued duration of jobs: " + str(round(totalQueuedDuration, 2)) + "seconds")
print("Oldest job date: " + str(oldestJobDate))
print("ID of oldest job: " + str(oldestJobId))



