import requests
import json
import sys

def getPrimaryMetrics(jobId):
    headers = {
        'PRIVATE-TOKEN': 'glpat-GWkAbhh-Vjkh-eksWKos',
    }
    # Volá Gitlab api a získává souhrná data o jobu, dle jobId který je předán jako parametr
    jobData = requests.get("https://gitlab.com/api/v4/projects/32057431/jobs/" + jobId, headers=headers)
    jobData = json.loads(jobData.text)

    # Ze získaného JSON objektu získáváme souhrná data o jobu    
    print("Duration: {:.2f}".format(jobData['duration']))
    print("Queued duration: {:.2f}".format(jobData['queued_duration']))
       
if __name__ == "__main__":
    getPrimaryMetrics(sys.argv[1])